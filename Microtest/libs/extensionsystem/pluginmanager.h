#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <QObject>

#include "extensionsystem_global.h"
#include "plugin.h"

class EXTENSIONSYSTEMSHARED_EXPORT PluginManager : public QObject
{
    Q_OBJECT
public:
    static PluginManager *instance();
    static void loadPlugins();
    static void initialize();
    
    static void addObject(QObject *obj);
    static void removeObject(QObject *obj);
protected:
    explicit PluginManager(QObject *parent = 0);
private:
    static QList<Plugin*> m_pluginsList;
    static QList<QObject*> m_allObject;
    static PluginManager *m_instance;
signals:
    void objectAdded(QObject *obj);
    void initializationDone();
};

#endif // PLUGINMANAGER_H
