#-------------------------------------------------
#
# Project created by QtCreator 2016-09-16T14:59:00
#
#-------------------------------------------------

QT       += widgets

TARGET = extensionsystem
TEMPLATE = lib

DEFINES += EXTENSIONSYSTEM_LIBRARY

DESTDIR = ../../Microtest/libs

SOURCES += extensionsystem.cpp \
    pluginmanager.cpp \
    plugin.cpp

HEADERS += extensionsystem.h\
        extensionsystem_global.h \
    pluginmanager.h \
    plugin.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
