#include "plugin.h"
#include "pluginmanager.h"


void Plugin::addAutoReleasedObject(QObject *obj)
{
    PluginManager::addObject(obj);
}

void Plugin::removeObject(QObject *obj)
{
    PluginManager::removeObject(obj);
}
