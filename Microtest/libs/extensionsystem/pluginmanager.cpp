#include "pluginmanager.h"

#include <QDir>
#include <QPluginLoader>
#include <QApplication>

#include <QWidget>
#include <QList>

#include <QDebug>

PluginManager::PluginManager(QObject *parent) : QObject(parent)
{
    
}

QList<Plugin*> PluginManager::m_pluginsList = QList<Plugin*>();
QList<QObject*> PluginManager::m_allObject;
PluginManager *PluginManager::m_instance = nullptr;

PluginManager *PluginManager::instance()
{
    if(!m_instance)
    {
        m_instance = new PluginManager();
    }
    return m_instance;
}

void PluginManager::loadPlugins()
{
    QDir pluginsDir(qApp->applicationDirPath());
#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS") 
    {
        pluginsDir.cdUp();
        pluginsDir.cdUp();
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("plugins");
    qDebug() << "[PluginManager::loadPlugins]" << pluginsDir.entryList(QDir::Files);
    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) 
    {
        QPluginLoader pluginLoader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = pluginLoader.instance();
        if (plugin) 
        {
            qDebug() << pluginLoader.fileName();
            Plugin *plug = qobject_cast<Plugin*>(plugin);
            if (plug)
            {
                m_pluginsList.append(plug);
            }
        }
    }
    qDebug() << "[PluginManager::loadPlugins]" << m_pluginsList.size();
}

void PluginManager::initialize()
{
    for(auto it : m_pluginsList)
    {
        it->initialize();
    }
}

void PluginManager::addObject(QObject *obj)
{
    m_allObject.append(obj);
    Q_ASSERT_X(m_instance != nullptr, "PluginManager::addObject", "instance is null");
    emit m_instance->objectAdded(obj);
}

void PluginManager::removeObject(QObject *obj)
{
    m_allObject.removeOne(obj);
}


