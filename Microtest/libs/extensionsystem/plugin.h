#ifndef PLUGIN_H
#define PLUGIN_H

#include <QObject>
#include "extensionsystem_global.h"

class EXTENSIONSYSTEMSHARED_EXPORT Plugin 
{
    
public:
    virtual bool initialize() = 0;
    void addAutoReleasedObject(QObject *obj);
    void removeObject(QObject *obj);
signals:
    
public slots:
};


#define Core_Plugin_iid "org.qt-project.Qt.Microtest.Core_Plugin"
Q_DECLARE_INTERFACE(Plugin, Core_Plugin_iid)
#endif // PLUGIN_H
