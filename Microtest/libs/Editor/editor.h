#ifndef EDITOR_H
#define EDITOR_H

#include "editor_global.h"

class EDITORSHARED_EXPORT Editor
{
    
public:
    Editor();
    static void test();
};

#endif // EDITOR_H
