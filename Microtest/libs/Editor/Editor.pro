#-------------------------------------------------
#
# Project created by QtCreator 2016-09-16T14:22:50
#
#-------------------------------------------------

QT       += widgets

TARGET = Editor
TEMPLATE = lib

DEFINES += EDITOR_LIBRARY

DESTDIR = ../../Microtest/libs

SOURCES += editor.cpp

HEADERS += editor.h\
        editor_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
