depfile = $$replace(_PRO_FILE_PWD_, ([^/]+$), \\1/\\1_dependencies.pri)
exists($$depfile) {
    include($$depfile)
    isEmpty(MT_PLUGIN_NAME): \
        error("$$basename(depfile) does not define MT_PLUGIN_NAME.")
} else {
    isEmpty(MT_PLUGIN_NAME): \
        error("MT_PLUGIN_NAME is empty. Maybe you meant to create $$basename(depfile)?")
}

TARGET = $$MT_PLUGIN_NAME

plugin_deps = $$MT_PLUGIN_DEPENDS
plugin_test_deps = $$MT_TEST_DEPENDS
plugin_recmds = $$MT_PLUGIN_RECOMMENDS

IDE_SOURCE_TREE = $$PWD

MT_PLUGIN_DIRS_FROM_ENVIRONMENT = $$(MT_PLUGIN_DIRS)
MT_PLUGIN_DIRS += $$split(MT_PLUGIN_DIRS_FROM_ENVIRONMENT, $$QMAKE_DIRLIST_SEP)
MT_PLUGIN_DIRS += $$IDE_SOURCE_TREE/plugin
for(dir, MC_PLUGIN_DIRS) {
    INCLUDEPATH += $$dir
}

defineReplace(dependencyName) {
    dependencies_file =
    for(dir, MT_PLUGIN_DIRS) { 
        exists($$dir/$$1/$${1}_dependencies.pri) {
            dependencies_file = $$dir/$$1/$${1}_dependencies.pri
            break()
        }
    }
    isEmpty(dependencies_file): \
        error("Plugin dependency $$dep not found")
    include($$dependencies_file)
    return($$MT_PLUGIN_NAME) 
}

dependencyList =
for(dep, plugin_deps) {
    dependencyList += "        { \"Name\" : \"$$dependencyName($$dep)\", \"Version\" : \"$$QTCREATOR_VERSION\" }"
}
for(dep, plugin_recmds) {
    dependencyList += "        { \"Name\" : \"$$dependencyName($$dep)\", \"Version\" : \"$$QTCREATOR_VERSION\", \"Type\" : \"optional\" }"
}
for(dep, plugin_test_deps) {
    dependencyList += "        { \"Name\" : \"$$dependencyName($$dep)\", \"Version\" : \"$$QTCREATOR_VERSION\", \"Type\" : \"test\" }"
}
dependencyList = $$join(dependencyList, ",$$escape_expand(\\n)")

dependencyList = "\"Dependencies\" : [$$escape_expand(\\n)$$dependencyList$$escape_expand(\\n)    ]"

PLUGINJSON = $$_PRO_FILE_PWD_/$${TARGET}.json
PLUGINJSON_IN = $${PLUGINJSON}.in
exists($$PLUGINJSON_IN) {
    DISTFILES += $$PLUGINJSON_IN
    QMAKE_SUBSTITUTES += $$PLUGINJSON_IN
    PLUGINJSON = $$OUT_PWD/$${TARGET}.json

} else {
    # need to support that for external plugins
    DISTFILES += $$PLUGINJSON
    
}

