#include "testview.h"

TestView::TestView(QWidget *parent) : QWidget(parent)
{
    vlay = new QVBoxLayout();
    vlay->addWidget(new QPushButton("button"));
    setLayout(vlay);
}

void TestView::addView(QWidget *widget)
{
    vlay->addWidget(widget);
}
