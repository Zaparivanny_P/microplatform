#ifndef IMAINPLANE_H
#define IMAINPLANE_H

#include <QObject>
class QWidget;

namespace Core {

    class IMainPlane
    {
    public:
        virtual ~IMainPlane() {}
        virtual QWidget* widget() = 0;
    signals:
        
    public slots:
    };
    

}
#define Core_IMainPlane_iid "org.qt-project.Qt.Microtest.Core_IMainPlane"
Q_DECLARE_INTERFACE(Core::IMainPlane, Core_IMainPlane_iid)

#endif // IMAINPLANE_H
