#-------------------------------------------------
#
# Project created by QtCreator 2016-09-15T12:20:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Microtest
TEMPLATE = app

INCLUDEPATH    += ../libs
LIBS += -L./libs/ -lEditor -lextensionsystem

SOURCES += main.cpp\
        mainwindow.cpp \
    Core/imainplane.cpp \
    testview.cpp

HEADERS  += mainwindow.h \
    Core/imainplane.h \
    testview.h
