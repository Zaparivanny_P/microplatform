#ifndef TESTVIEW_H
#define TESTVIEW_H

#include <QWidget>
#include <QLabel>
#include <QLayout>
#include <QPushButton>

class TestView : public QWidget
{
    Q_OBJECT
public:
    explicit TestView(QWidget *parent = 0);
    void addView(QWidget *widget);
private:
    QVBoxLayout *vlay;
signals:
    
public slots:
};

#endif // TESTVIEW_H
