#include "mainwindow.h"

#include "testview.h"
#include "extensionsystem/pluginmanager.h"
#include "Editor/editor.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    
    PluginManager::instance();
    PluginManager::loadPlugins();
    
    TestView *widget = new TestView();
    
    
    PluginManager::initialize();
    
    Editor::test();
    
    
    setCentralWidget(widget);
}

MainWindow::~MainWindow()
{
    
}
