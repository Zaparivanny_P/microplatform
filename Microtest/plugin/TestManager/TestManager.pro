#-------------------------------------------------
#
# Project created by QtCreator 2016-09-16T11:52:24
#
#-------------------------------------------------

include(../../platformplugin.pri)

QT       += widgets

TARGET = TestManager
TEMPLATE = lib
CONFIG += plugin
INCLUDEPATH    += ../../libs
DESTDIR         = ../../Microtest/plugins

LIBS += -L../../Microtest/libs -lEditor -lextensionsystem


SOURCES += testmanager.cpp

HEADERS += testmanager.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
