#ifndef TESTMANAGER_H
#define TESTMANAGER_H

#include <QObject>
#include "extensionsystem/plugin.h"

class Q_DECL_EXPORT TestManager : public QObject, Plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.Microtest.TestManager")
    Q_INTERFACES(Plugin)
public:
    
    virtual bool initialize() override;
    static void test();
public slots:
    void objectAdded(QObject* obj);
};

#endif // TESTMANAGER_H
