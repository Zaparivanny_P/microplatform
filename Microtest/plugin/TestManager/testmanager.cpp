#include "testmanager.h"
#include <QDebug>
#include <QPushButton>

#include "extensionsystem/pluginmanager.h"
#include "testmanager.h"

bool TestManager::initialize()
{
    qDebug() << "[TestManager::initialize]";
    connect(PluginManager::instance(), SIGNAL(objectAdded(QObject*)), SLOT(objectAdded(QObject*)));
    addAutoReleasedObject(new QPushButton("TestManager"));
    return true;
}

void TestManager::test()
{
    qDebug() << "[TestManager::test]";
}

void TestManager::objectAdded(QObject *obj)
{
    qDebug() << "[TestManager::objectAdded]";
}
