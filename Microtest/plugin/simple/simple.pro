
include(../../platformplugin.pri)

TEMPLATE        = lib
CONFIG         += plugin
QT             += widgets

INCLUDEPATH    += ../../libs
INCLUDEPATH    += ../TestManager
INCLUDEPATH    += ../../libs/Editor


HEADERS         = simple.h
SOURCES         = simple.cpp
TARGET          = $$qtLibraryTarget(simple)
DESTDIR         = ../../Microtest/plugins

LIBS += -L$$DESTDIR -lTestManager
LIBS += -L../../Microtest/libs -lEditor -lextensionsystem

EXAMPLE_FILES = simple.json

# install
#target.path = $$(PWD)../
#INSTALLS += target

CONFIG += install_ok  # Do not cargo-cult this!
