#include <QtWidgets>

#include "simple.h"
#include <QPushButton>
#include <QDebug>
#include "testmanager.h"
#include "editor.h"

bool Simple::initialize()
{
    qDebug() << "[Simple::initialize]";
    Editor::test();
    //TestManager::test();
    addAutoReleasedObject(new QPushButton("Тест"));
    //Core::Plugin::addAutoReleasedObject(nullptr);
    return true;
}
