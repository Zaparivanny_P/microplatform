#ifndef ECHOPLUGIN_H
#define ECHOPLUGIN_H

#include <QObject>
#include "extensionsystem/plugin.h"

class Simple : public QObject, Plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.Microtest.Core_Plugin" FILE "simple.json")
    Q_INTERFACES(Plugin)

public:
    Simple(){
        
    }

    virtual bool initialize() override;
};


#endif
