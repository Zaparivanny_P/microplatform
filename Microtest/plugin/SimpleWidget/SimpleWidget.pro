#-------------------------------------------------
#
# Project created by QtCreator 2016-09-15T13:06:41
#
#-------------------------------------------------

QT       += widgets

TARGET = SimpleWidget
TEMPLATE = lib

CONFIG         += plugin
INCLUDEPATH    += ../../Microtest
DESTDIR         = ../../Microtest/plugins

DEFINES += SIMPLEWIDGET_LIBRARY

SOURCES += simplewidget.cpp

HEADERS += simplewidget.h\
        simplewidget_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

CONFIG += install_ok
