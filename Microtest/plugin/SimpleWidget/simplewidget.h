#ifndef SIMPLEWIDGET_H
#define SIMPLEWIDGET_H

#include "simplewidget_global.h"
#include "Core/imainplane.h"

//using namespace Core;

class SimpleWidget : public QObject, Core::IMainPlane
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.Microtest.Core_IMainPlane" FILE "plugin.json")
    Q_INTERFACES(Core::IMainPlane)
    
public:
    virtual QWidget *widget() override;
    
};

#endif // SIMPLEWIDGET_H
